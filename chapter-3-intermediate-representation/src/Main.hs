module Main where

import System.Environment
import Control.Category ((>>>))
import FrontEnd.Parser (strToTree)
import BackEnd.IR (treeToIR)


main :: IO ()
main = do
  input <- getArgs
  let
    inputProgram = input !! 0
    handleErr = show >>> putStrLn
    compile = treeToIR >>> show >>> putStrLn
    in
      either handleErr compile (strToTree inputProgram)
