module BackEnd.IR where

import FrontEnd.Parser (Expr(..), BoolOp(..))

data StackOp
  = Push Bool
  | Op BoolOp
  deriving (Show)

type IntermediateForm = [StackOp]

treeToIR :: Expr -> IntermediateForm
treeToIR tree = treeToIR' [] tree

treeToIR' :: [StackOp] -> Expr -> [StackOp]
treeToIR' stack node =
  case node of
    ExprLeaf ident -> Push ident : stack
    ExprBranch op exprA exprB -> opStackFromBranch stack op exprA exprB

opStackFromBranch :: [StackOp] -> BoolOp -> Expr -> Expr -> [StackOp]
opStackFromBranch stack op exprA exprB =
  let
    branchA = treeToIR' [] exprA
    branchB = treeToIR' [] exprB
    in
      stack ++ (Op op : (branchB ++ branchA))
