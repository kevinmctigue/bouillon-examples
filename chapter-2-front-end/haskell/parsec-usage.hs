module Main where

import System.Environment
import Text.Parsec ((<|>), parse, skipMany1, space)
import Text.Parsec.String (Parser)
import Text.Parsec.Char (char, spaces, string)


data Op
  = And
  deriving (Show)

data Expr
  = ExprLeaf Bool
  | ExprBranch Op Expr Expr -- Now an Expr can be composed of other Exprs!
  deriving (Show)


identParser :: Parser Expr
identParser =
  (string "#t") >>= \identStr ->
    return (ExprLeaf True)

opParser :: Parser Op
opParser =
  (string "AND") >>= \opStr ->
    return And

exprParser :: Parser Expr
exprParser = do
  char '('
  op <- opParser
  skipMany1 space
  exprA <- (identParser <|> exprParser)
  skipMany1 space
  exprB <- (identParser <|> exprParser)
  char ')'
  return (ExprBranch op exprA exprB)

main :: IO ()
main = do
  input <- getArgs
  case (parse exprParser "" (input !! 0)) of
    Left err -> putStrLn ("No match: " ++ show err)
    Right val -> putStrLn (show val)
