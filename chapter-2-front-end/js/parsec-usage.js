
const { parserCombinator } = require('./parsec');

const Ident = ident => ({ type: 'Ident', value: ident });
const Op = op => ({ type: 'Op', value: op });


const identParser = parserCombinator(parse =>
  parse(/^#t/).then(identStr =>
    Ident('true')
  )
);

const opParser = parserCombinator(parse =>
  parse(/^AND/).then(opStr =>
    Op('and')
  )
);

const spaceParser = parserCombinator(parse =>
  parse(/^\s+/).then(() => null)
);

const exprParser = ctx =>
  opParser(ctx)
  .then(spaceParser)
  .then(identParser)
  .then(spaceParser)
  .then(identParser);

const twoExprParser = ctx =>
  exprParser(ctx)
  .then(spaceParser)
  .then(exprParser);


function main(input) {
  const startingCtx = {
    input, // input: process.argv[2],
    tokens: [],
    loc: 0
  };

  twoExprParser(startingCtx)
  .then(console.log)
  .catch(console.log);
}

main('AND #t #t AND #t #t');
