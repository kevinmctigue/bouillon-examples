
// Takes a parser function (like identP) and wraps it in another function that:
//
// 1. Takes in a ctx object from a previous parser
// 2. Runs the given `parser` using the `ctx` object
// 3. Takes the result of that parser and advances `loc`
// 4. Adds the `token` from `parser` to the list of `tokens`
// 5. Passes the updated `ctx` object on with `.then` for the next parser
//
// In short, it abstracts away the management of `ctx` so our parser functions
// don't have to deal with it; they can focus on parsing.
exports.parserCombinator = parser => ctx => {
  const { input, tokens, loc } = ctx;
  const str = input.slice(loc);
  const newCtx = Object.assign({}, ctx);

  // If there's no more `str`, we've reached the end and are done parsing
  if (!str) return Promise.resolve(ctx);

  // `check` is given to this function by running `parser`
  return parser(check => {
    // If it fails the test right at the beginning, it means the given string
    // doesn't fit with our order of parsers
    if (!check.test(str)) {
      return Promise.reject(new Error(
        `Unexpected char ${str[0]} at col: ${loc + 1}`
      ));
    }

    const matched = str.match(check)[0];

    // Advance `loc` by the number of matched characters so that the next
    // parser starts where this one ended
    newCtx.loc = ctx.loc + matched.length;

    return Promise.resolve(matched);
  })
  .then(token => {
    // Add `token` to the whole list of `newCtx.tokens`
    if (token) newCtx.tokens = tokens.concat(token);

    return newCtx;
  })
};
