module FrontEnd.Parser where

import Text.Parsec ((<|>), parse, skipMany1, space)
import Text.Parsec.String (Parser)
import Text.Parsec.Char (char, spaces, string)
import Text.Parsec.Error (ParseError)


data BoolOp
  = And
  | Or
  deriving (Show)

data Expr
  = ExprLeaf Bool
  | ExprBranch BoolOp Expr Expr -- Now an Expr can be composed of other Exprs!
  -- deriving (Show)

instance Show Expr where
  show = printTree

printTree :: Expr -> String
printTree branch = printTree' branch 0
  where
    printTree' x depth = case x of
      ExprBranch op exprA exprB -> foldl (++) ""
        [ replicate (3 * depth) ' '
        , show op
        , "\n"
        , printTree' exprA (depth + 1)
        , "\n"
        , printTree' exprB (depth + 1)
        ]
      ExprLeaf ident -> foldl (++) ""
        [ replicate (3 * depth) ' '
        , show ident
        ]


identParser :: Parser Bool
identParser = do
  char '#'
  identStr <- char 't' <|> char 'f'
  case identStr of
    't' -> return True
    'f' -> return False

opParser :: Parser BoolOp
opParser = do
  opStr <- (string "AND" <|> string "OR")
  case opStr of
    "AND" -> return And
    "OR" -> return Or

exprLeafParser :: Parser Expr
exprLeafParser =
  identParser >>= \ident ->
    return (ExprLeaf ident)

exprParser :: Parser Expr
exprParser = do
  char '('
  op <- opParser
  skipMany1 space
  exprA <- (exprLeafParser <|> exprParser)
  skipMany1 space
  exprB <- (exprLeafParser <|> exprParser)
  char ')'
  return (ExprBranch op exprA exprB)

strToTree :: String -> Either ParseError Expr
strToTree =
  parse exprParser ""
