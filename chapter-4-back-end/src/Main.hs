module Main where

import System.Environment
import Control.Category ((>>>))
import FrontEnd.Parser (strToTree)
import BackEnd.IR (treeToIR)
import BackEnd.WAT (compileToWAT)


main :: IO ()
main = do
  input <- getArgs
  let
    inputProgram = input !! 0
    filePath = input !! 1
    handleErr = show >>> putStrLn
    compile = treeToIR >>> compileToWAT >>> writeFile filePath
    in
      either handleErr compile (strToTree inputProgram)
