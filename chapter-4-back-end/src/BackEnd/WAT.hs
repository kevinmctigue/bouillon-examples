module BackEnd.WAT where

import Control.Category ((>>>))
import BackEnd.IR (StackOp(..), IntermediateForm)
import FrontEnd.Parser (BoolOp(..))

type StackCode = [String]

wrapWATStack stack =
  unlines
    [ "(module"
    , "  (func (export \"main\") (result i32)"
    , stack ++ "))"
    ]

compileToWAT :: IntermediateForm -> String
compileToWAT =
  irToStackCode
  >>> formatStackCode 2
  >>> stackCodeToWATStr
  >>> wrapWATStack

formatStackCode :: Int -> StackCode -> StackCode
formatStackCode depth stack =
  let indent = replicate (2 * depth) ' ' -- 2 spaces for every indent depth
    in map (\a -> indent ++ a) stack

stackCodeToWATStr :: StackCode -> String
stackCodeToWATStr =
  -- foldr (\line watStr -> watStr ++ line) ""
  --   foldr (flip (++)) ""
  reverse >>> unlines

irToStackCode :: IntermediateForm -> StackCode
irToStackCode = map irToWATMappings

irToWATMappings :: StackOp -> String
irToWATMappings stackOp =
  case stackOp of
    Push True -> "i32.const 1"
    Push False -> "i32.const 0"
    Op And -> "i32.and"
    Op Or -> "i32.or"
